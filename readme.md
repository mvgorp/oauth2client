# oAuth2 Client for Laravel Passport API's

Example code:

```php
// Autoloader
require 'vendor/autoload.php';

/**
 * Set credentials
 */
define('BASE_URI', '');
define('CLIENT_ID', 0);
define('CLIENT_SECRET', '');
define('USERNAME', '');
define('PASSWORD', '');
define('PERSONAL_ACCESS_TOKEN', '');

// Instantiate
$client = new \Intermax\Laravel\OAuth2Client();
$client->setBaseUri(BASE_URI);

/**
 * Two methods of authentication
 */

/*
 * 1) Password Grant Client
 */
$client = new \Intermax\Laravel\OAuth2Client();
$response = $client->setBaseUri(BASE_URI)
    ->startPasswordGrantClient(CLIENT_ID, CLIENT_SECRET, USERNAME, PASSWORD)
    ->get('/api/user');
//
echo (string)$response->getBody();

/*
 * 2) Start with a Personal Access Client
 */
$client = new \Intermax\Laravel\OAuth2Client();
$client->setBaseUri(BASE_URI)
    ->startPersonalAccessClient(PERSONAL_ACCESS_TOKEN)
    ->get('/api/user');
//
echo (string)$response->getBody();
```



