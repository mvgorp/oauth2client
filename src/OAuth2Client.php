<?php namespace Intermax\Laravel;

use GuzzleHttp\Client as GuzzleClient;


/**
 * Class OAuth2Client
 *
 * @package laravel
 */
class OAuth2Client extends GuzzleClient {

    /**
     * @var array
     */
    protected $guzzleConfig;

    /**
     * @var string
     */
    protected $access_token;

    /**
     * @var string
     */
    protected $refresh_token;

    /**
     * @var bool
     */
    protected $verify_https = false;

    /**
     * oAuth2Client constructor.
     *
     * @param string $base_uri
     */
    public function __construct($base_uri = '')
    {
        //
        $this->guzzleConfig = [
            'verify' => $this->verify_https
        ];

        $this->setBaseUri($base_uri);

    }

    /**
     * Set URL and re-init Guzzle Client
     *
     * @param $base_uri
     * @return $this
     */
    public function setBaseUri($base_uri) {

        //
        if(!empty($base_uri))
            $this->guzzleConfig['base_uri'] = $base_uri;

        //
        parent::__construct($this->guzzleConfig);

        //
        return $this;
    }

    /**
     * Connect to API with Password Grant credentials
     * 
     * @param $client_id
     * @param $client_secret
     * @param $username
     * @param $password
     * @param $scope
     *
     * @return $this
     */
    public function startPasswordGrantClient($client_id, $client_secret, $username, $password, $scope = null) {

        //
        $options = [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => (int)$client_id,
                'client_secret' => $client_secret,
                'username' => $username,
                'password' => $password,
                'scope' => $scope
            ]
        ];

        // Fetch
        $response = $this->post('/oauth/token', $options);

        // Get bodydata in JSON
        $json = (string)$response->getBody();

        // Decode JSON
        $data = \GuzzleHttp\json_decode($json, true);

        // Check access token
        if(!empty($data['access_token']))
            $this->access_token = $data['access_token'];

        // Check refresh token
        if(!empty($data['refresh_token']))
            $this->refresh_token = $data['refresh_token'];

        //
        $this->prepareClient();

        //
        return $this;

    }

    /**
     * Connect to API with Personal Access Token
     *
     * @param $access_token
     *
     * @return $this
     */
    public function startPersonalAccessClient($access_token) {

        // Set
        $this->access_token = $access_token;
        $this->refresh_token = '';

        //
        $this->prepareClient();
        
        //
        return $this;
    }


    /**
     * Prepare Guzzle Client for following requests
     *
     */
    private function prepareClient() {

        // Set default headers
        $this->guzzleConfig['headers'] = [
            'Authorization' => 'Bearer ' . $this->access_token,
            'Accept' => 'application/json'
        ];

        //
        parent::__construct($this->guzzleConfig);
    }


}